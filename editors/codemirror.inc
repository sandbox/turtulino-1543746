<?php
/**
 * Plugin implementation of hook_editor().
 */
function code_editor_codemirror_editor() {
  $editor['codemirror'] = array(
    'title'                    => 'CodeMirror',
    'vendor url'               => 'http://codemirror.net/',
    'download url'             => 'http://codemirror.net/codemirror.zip',
    'libraries'                => array(
      '' => array(
        'title' => 'Default',
        'files' => array(
          'lib/codemirror.js'             => array('preprocess' => FALSE),
          'mode/xml/xml.js'               => array('preprocess' => FALSE),
          'mode/javascript/javascript.js' => array('preprocess' => FALSE),
          'mode/css/css.js'               => array('preprocess' => FALSE),
          'mode/htmlmixed/htmlmixed.js'   => array('preprocess' => FALSE),
        ),
      ),
    ),
    'version callback'         => 'code_editor_codemirror_version',
    'themes callback'          => 'code_editor_codemirror_themes',
    'settings callback'        => 'code_editor_codemirror_settings',
    'plugin callback'          => 'code_editor_codemirror_plugins',
    'plugin settings callback' => 'code_editor_codemirror_plugin_settings',
    //'proxy plugin settings callback' => 'code_editor_codemirror_proxy_plugin_settings',
    'versions'                 => array(
      '2' => array(
        'js files'  => array('codemirror.js'),
        'css files' => array('codemirror.css'),
      ),
    ),
  );
  return $editor;
}

/**
 * Detect editor version.
 *
 * @param $editor
 *   An array containing editor properties as returned from hook_editor().
 *
 * @return
 *   The installed editor version.
 */
function code_editor_codemirror_version($editor) {

  return '2';

  //  $library = $editor['library path'] . '/foo.js';
  //  if (!file_exists($library)) {
  //    return;
  //  }
  //  $library = fopen($library, 'r');
  //  $max_lines = 8;
  //  while ($max_lines && $line = fgets($library, 500)) {
  //    // version:'CKEditor 3.0 SVN',revision:'3665'
  //    // version:'3.0 RC',revision:'3753'
  //    // version:'3.0.1',revision:'4391'
  //    if (preg_match('@version:\'(?:CKEditor )?([\d\.]+)(?:.+revision:\'([\d]+))?@', $line, $version)) {
  //      fclose($library);
  //      // Version numbers need to have three parts since 3.0.1.
  //      $version[1] = preg_replace('/^(\d+)\.(\d+)$/', '${1}.${2}.0', $version[1]);
  //      return $version[1] . '.' . $version[2];
  //    }
  //    $max_lines--;
  //  }
  //  fclose($library);
}

/**
 * Return runtime editor settings for a given wysiwyg profile.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $config
 *   An array containing wysiwyg editor profile settings.
 * @param $theme
 *   The name of a theme/GUI/skin to use.
 *
 * @return
 *   A settings array to be populated in
 *   Drupal.settings.wysiwyg.configs.{editor}
 */
function code_editor_codemirror_settings($editor, $config, $theme) {
  drupal_add_css($editor['library path'] . '/lib/codemirror.css', array( 'group' => CSS_THEME ));

  drupal_add_css($editor['library path'] . '/theme/default.css', array(
    // Specify an alternate basename; otherwise, default.css would override a
    // commonly used default.css file.
    'basename' => 'codemirror.' . $theme . '.style.css',
    'group' => CSS_THEME,
  ));
}


/**
 * Return internal plugins for this editor; semi-implementation of hook_wysiwyg_plugin().
 */
//function code_editor_codemirror_plugins($editor) {
//  return array(
//    'default' => array(
//    ),
//  );
//}


/**
 * Determine available editor themes or check/reset a given one.
 *
 * @param $editor
 *   A processed hook_editor() array of editor properties.
 * @param $profile
 *   A wysiwyg editor profile.
 *
 * @return
 *   An array of theme names. The first returned name should be the default
 *   theme name.
 */
function code_editor_codemirror_themes($editor, $profile) {
  return array('default', 'elegant', 'neat', 'night');
}


function code_editor_codemirror_plugin_settings(){}
