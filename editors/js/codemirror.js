(function($) {

Drupal.codeEditor = {};

/**
 * Attach this editor to a target element.
 */
Drupal.wysiwyg.editor.attach.codemirror = function(context, params, settings) {
// $('#' + params.field, context).hide();
   Drupal.codeEditor.codeMirror = 
	   CodeMirror.fromTextArea($('#' + params.field, context)[0],
	       {mode: "htmlmixed", 
		lineNumbers: true,
		onCursorActivity: function() {
		       editor.setLineClass(hlLine, null);
		       hlLine = editor.setLineClass(editor.getCursor().line, "activeline");
		 }
	       }
	   );
//
// // Adjust CSS for editor buttons.
// $.each(settings.markupSet, function (button) {
// $('.' + settings.nameSpace + ' .' + this.className + ' a')
// .css({ backgroundImage: 'url(' + settings.root + 'sets/default/images/' +
// button + '.png' + ')' })
// .parents('li').css({ backgroundImage: 'none' });
// });
};

/**
 * Detach a single or all editors.
 */
Drupal.wysiwyg.editor.detach.codemirror = function(context, params) {
  
  Drupal.codeEditor.codeMirror.toTextArea();
  
  $('#' + params.field, context).show();
// if (typeof params != 'undefined') {
// $('#' + params.field, context).markItUpRemove();
// }
// else {
// $('.markItUpEditor', context).markItUpRemove();
// }
};

})(jQuery);
